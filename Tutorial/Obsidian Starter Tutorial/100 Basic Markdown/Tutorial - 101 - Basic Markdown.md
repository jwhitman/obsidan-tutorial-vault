

## 101 - Introduction to Markdown 

#Obsidian works mainly by rendering text files with the `.md` extension with styling. Markdown is a way of writing HTML without writing HTML. The focus of markdown is to make writing a styled document very fast by minimizing the amount of typing you have to do. 

> **Note**: You should feel free to edit this document as you work through it. You can try things out, change things could be stated better, or make it entirely your own. It's up to you. 

There is a strong focus in apps like Obsidian on being able to enter information quickly, without having to take your hands off the keyboard. Markdown is pretty well suited to this task because there is not the much styling to learn, and not very many characters to type. 

### HTML VS MARKDOWN

Here is the same heading and paragraph twice, in raw HTML and then Markdown. 

```html
<h1>A Main Heading</h1>

<p>This is a paragraph. I have added a <strong>bold</strong> word and a <a href="google.com">link</a></p>. 
```  

```markdown 
# A Main Heading

This is a paragraph. I have added a **bold** word and a [link](www.google.com).
```

The keystroke savings are obvious. 

___

## 102 - Editing Markdown 

Obsidians' default mode is the render the markdown only when it is being edited. 

To see the markdown for any of the examples below simply move the cursor to each line or inline example. 

## 103 - Markdown Examples: 

### Headings 

(The headings have been escaped in this instance with the `\` character to preserve the outline of the document.)

\# Heading 1
\## Heading 2
\### Heading 3
\#### Heading 4
\##### Heading 5
\###### Heading 6

### Paragraphs

This is an example of a paragraph. It has no markup. It is a paragraph because it is on a single line with no markup.  

### Inline-Styles

This is also an example of a paragraph,  but with **bold** and *italic* and ***bold+ittalic*** words in it. You can also do [[Tutorial -  101 - Basic Markdown - Link Here File|Internal Links]] and ~~strikethrough~~~. 

You can also **~~~mix~~~** and ~~*match*~~. 

### Links 

### Links - Internal

Here is an internal link to a note called "[[Tutorial -  101 - Basic Markdown - Link Here File]]".

Here is an internal link to a note called  link here, but renamed "[[Tutorial -  101 - Basic Markdown - Link Here File|Link Here Renamed]]".

Here is an external link to the lofi-hip-ho youtube. [Lof-Fi Hip Hop YouTube](https://www.youtube.com/live/jfKfPfyJRdk)

Here is a link to an image on the [internet](https://picsum.photos/200). 

Here is a rendered image located on the internet:

![](https://picsum.photos/200)

### Tags 

#Tags can be added with a hashtag followed directly by a word.  You can also use YAML Frontmatter to add tags in another fashion. 

- [ ] Add YAML Article and link above. 

##  Lists

Lists are created by a leading dash and a space.  

### Lists - Basic

Common Greetings: 
- Hello 
- Bonjour 
- Hi
- What's up


### Lists - Numbered

Shopping List:
1. Milk
2. Whiskey
3.  Milk duds

### Lists - Indented/Embedded Lists

Shipping List: 
1. Milk
	- 1/2 gallon
2. Whiskey
	- 3 Bottles
3. Milk Duds
	- 16 cases

### Lists - Todos

Todos or Checkboxes are created with a `- [ ]`.
Completed Todos look like - [x]

- [x] Sweep
- [x] Vacuum
- [ ] Mop
- [ ] Chastise animals for shedding

## Citations

> This is a citation/quote text. This is useful for information from a source or a quote from an article...or honestly whatever you want to use it for in your notes.  



## Code

Code text, text you'd like to be rendered completely as written and styled as `code` can be written in two ways. You can use `backticks` for inline code, or you can use  3x backticks on it's own line for a `codeblock`. 

Here some sample markdown as a code block. 

```markdown[Example]

# Introduction to Markdown 

## Markdown Examples: 

### Inline-Styles

This is also an example of a paragraph,  but with **bold** and *italic* and ***bold+ittalic*** words in it. You can also do [[Link Here|Internal Links]] 

```

Note that codeblocks also have a copy button in the top right corner. **This will copy the entire contents of the code block into your clipboard as raw text.** 

```python
print("hello world")
```

## Tables

Tables in markdown are made with a specail laout of characters. 

```markdown
| Header Col 1 | Header Col 2 |
| ------------ | ------------ |
| content      | mo content   | 
```

| Header Col 1 | Header Col 2 |
| ------------ | ------------ |
| content      | mo content   |

Obviously that's too much to type, and impossible for a human to maintain as they add rows and columns and data to their tables. 

This is the first piece of formatting in this document that uses an #Obsidian-Plugin to add functionality. In this case we are using the Advanced Tables Community plugin, which autoformats tables for us. 

To start a table:
- Make sure community plugins are enabled in the [[Tutorial 201 - Obsidian Basics#Obsidian Settings|Obsidian Settings]].  
- Make sure the Advanced Tables Plugin is also enabled. 
- On the next line type  `|Cheese|` character and press `tab`

You should now have a table that looks like this:
| Cheese |     | 
| ------ | --- |

In the document it actually looks like this: 
```markdown
| Cheese |     | 
| ------ | --- |
```

All that table formatting is handled for you for free by the plugin, and you can add to the table as well. Each time you hit tab it will update the spacing for the whole table. 

Try to make the following table: 
| Cheese | Year | Country   | Featues |
| ------ | ---- | --------- | ------- |
| Swiss  | 2020 | Swissland | Holy    |

Good job. Now onto [[Tutorial 201 - Obsidian Basics]]!