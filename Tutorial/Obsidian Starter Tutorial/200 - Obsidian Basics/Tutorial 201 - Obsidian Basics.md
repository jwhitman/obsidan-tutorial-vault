
## The Obsidian Vault 

- An #Obsidian-Note is a text document with a `.md` file extension. 
- An #Obsidian-Vault is a **folder** on your **computer**. 
- An Obsidian Vault can contain anything, but is primarily concerned with:
	- #Markdown  
	- Media Files
	- Configuration/Settings files.
		- These are in a hidden folder called `.Obsidian` in your vault. 
		- This folder contains files that control things like:
			- Settings
			- Plugins
			- Themes
- Below is an image showing the location of the first document made for this vault, the [[Tutorial - 101 - Basic Markdown]] document, shown open in Obsidian, File Explorer, and  another Notepad. 
	- Hint: If the image is too small, press `ctrl +` repeatedly to increase the #Obsidian-Interface-Size. 

![[Pasted image 20230305105034.png]]

- The vault will house any files you care to add to it, but Obsidian might not be able to display and interact with all of them. This is often where #Obsidian-Plugins come in. 
- You can think of your vault as a webapp that you run locally, because that's basically what it is. 

## Obsidian Notes

- "Notes" are arguably that atomic units that make up your vault as a whole. 
- Each note in Obsidian is a Markdown text document.  
- Each note in must be located inside the **Vault Folder**, or a subfolder. 
- Obsidian tracks all saved changes to files in the vault. 
	- So a note edited in another application will instantly be updated by Obsidian as well once the changes are saved 

Here you can see the tutorial file open and an **unsaved change** made to it in notepad. 
 ![[Pasted image 20230305111519.png]]
The "VS" will be changed to "Vs" once the file is saved. 
![[Pasted image 20230305111932.png]]

Here's another change, with a **Close-up Before Saving:** 

![[Pasted image 20230305112000.png]]
**Closeup After Save:**
![[Pasted image 20230305112058.png]]

To that end I have added the #Obsidian-Community-Plugin [Welcome to VS Code](https://github.com/NomarCub/obsidian-open-vscode). This allows us to open the vault folder in VSCode as well, which gives us access to the hidden .Obsidian folder containing the configuration files for our Vault. 

![[Pasted image 20230305114049.png]]This allows us to edit our settings files by hand should we choose to.

# Obsidian Settings

You can also open settings the settings dialog in a variety of ways: 
- Click the gear icon in the lower left
	- ![[Pasted image 20230305114306.png]]
- The #Obsidian-Keyboard-Shortcut **`control + ,`**
- Using the command pallet
	- **`control + p`**
	- Type: "settings"
	- Select "**Open Settings**"
		- ![[Pasted image 20230305115048.png]]

## Obsidian Settings Dialog
![[Pasted image 20230305114628.png]]

The obsidian settings dialog allows you to change settings, as well as locate plugins and themes and install them. 

Changes made to settings files will be reflected in these text files, and vice versa. Here you can the setting's I've changed so far. 

![[Pasted image 20230305114223.png]]

Here is the option for `alwaysUpdateLinks` in the settings dialog. 

![[Pasted image 20230305115248.png]]

`attachmentFolderPath`:

![[Pasted image 20230305115343.png]]

`spellcheck`:
![[Pasted image 20230305115413.png]]
You can use window snapping to watch file changes in the Obsidian Settings Dialog and the `.Obsidian/app.json` file side by side. 

![[SettingsSideBySide.mov]]